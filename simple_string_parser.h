// alexkazancev@bk.ru 28-29 okt 2020
// simple string parser for arduino 

// If this code is usuful for you donate please, https://paypal.me/alvlk , https://yoomoney.ru/to/410019721796762

#define DEBUG false
#define parameters_count 10

struct ssp_struct 
{
 char delimiter = ','; //delimiter between expressions
 char c; //temp char for console reading procedure
 String temp; //temp string for one expression
 String string; //inputed string
 int count; //counter of inputed expressions
 String names [parameters_count]; //names of inputed parameters
 String values [parameters_count]; //values of inputed parameters
 double numerical_values [parameters_count]; //numerical values of parameters
};

void print_new_values(ssp_struct &S){
      if (S.count==0){
	Serial.println("Nothing");
      }
      else {
       for (int i=0; i<S.count; i++){
	Serial.print(S.names[i]);
        Serial.print("=");
        Serial.print(S.values[i]);
        Serial.print(", num_value=");
        Serial.println(S.numerical_values[i]);
       }
      }
}

void parser(ssp_struct &S){
    int j = 0; //last delimiter
    int k = 0; //position of "="
    int len = 0; //length of input string
    S.count = 0;
    S.string.replace(" ",""); 
    //S.string.toUpperCase();
    j=0;
    len=S.string.length();
    #if DEBUG ==true 
      Serial.println(len);
    #endif
    for (int i=0; i<=len; i++){
     //Serial.println(S.string[i]);
     //m=S.temp.indexOf(
     if ((S.string[i]==S.delimiter) || (i==len)) {
      S.temp=S.string.substring(j,i);
      #if DEBUG ==true 
        Serial.println(S.temp);
      #endif
      k=S.temp.indexOf('=');
      if (k>0 && k< S.temp.length()) {
       S.names[S.count]=S.temp.substring(0,k);
       #if DEBUG ==true
        Serial.println(S.names[S.count]);
       #endif
       S.values[S.count]=S.temp.substring(k+1,S.temp.length());
       #if DEBUG ==true
        Serial.println(S.values[S.count]);
       #endif
       S.numerical_values[S.count]=S.values[S.count].toFloat();
       #if DEBUG ==true
        Serial.println(S.numerical_values[S.count]);
       #endif
      }
      else {
       #if DEBUG ==true
        Serial.println("else");
       #endif
       S.names[S.count]=S.temp;
       #if DEBUG ==true
        Serial.println(S.temp);
       #endif
       S.values[S.count]="";
       S.numerical_values[S.count]=0;
      }
      S.count++;
      j=i+1;
     }
    }
    S.string="";
}
