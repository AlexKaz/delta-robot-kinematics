// see https://habr.com/ru/post/390281/ Кинематика дельта-робота
// http://www.cim.mcgill.ca/~paul/clavdelt.pdf Paul Zsombor-Murray Descriptive Geometric Kinematic Analysis of Clavel’s «Delta» Robot

// If this code is usuful for you donate please, https://paypal.me/alvlk , https://yoomoney.ru/to/410019721796762

#include "simple_string_parser.h"

ssp_struct S;

String kin;
float t1,t2,t3, x0,y0,z0; //три угла для прямой задачи и три координаты точки руки для обратной

void setup() { // самая первая функция пользователя, которая запускается на МК
 Serial.begin(9600); // запускаем обмен данными между ПК и МК
 Serial.println("test of simple string parser"); // пишем из МК в консоль ПК
 Serial.println("Print for example: a=2, b=-1, c=12.5, d=true, e=-1.2e5");
}

void loop() { // бесконечных цикл, запускается после setup()
 if (Serial.available())  { // ждём на МК данные из ПК
  S.c = Serial.read();  //получаем один байт из МК
  if (S.c == '\n') { // если строка завершается
   parser(S); // парсим полученную строку на МК (с помощью библиотеки SSP)
   print_new_values(S); // (с помощью библиотеки SSP) выводим распарсенные значения, передавая итог из МК в ПК на консоль 
  } 
  else {     
   S.string += S.c; //добавляем полученный байт к строке
  }
 }

 if (S.count>0){
  kin=S.names[0]; // определяем тип задачи по первой переменной из консоли
  
  if (kin=="di") { // прямая задача
    Serial.println("direct kinematics");
    t1=S.numerical_values[1];
    t2=S.numerical_values[2];
    t3=S.numerical_values[3];
    delta_calcForward(t1,t2,t3,x0,y0,z0);
    Serial.println(x0);
    Serial.println(y0);
    Serial.println(z0);
  }; 
  if (kin=="in") { //обратная задача
    Serial.println("inverse kinematics");
    x0=S.numerical_values[1];
    y0=S.numerical_values[2];
    z0=S.numerical_values[3];
    delta_calcInverse(x0,y0,z0,t1,t2,t3);
    Serial.println(t1);
    Serial.println(t2);
    Serial.println(t3);  
  };
  S.count=0;
 }
}

// размеры робота
// (обозначения см. на схеме)
const float e = 115.0;     // сторона подвижной платформы
const float f = 457.3;     // сторона неподвижного основания
const float re = 232.0;
const float rf = 112.0;

// тригонометрические константы
const float sqrt3 = sqrt(3.0);
const float pi = 3.141592653;    // PI
const float sin120 = sqrt3/2.0;
const float cos120 = -0.5;
const float tan60 = sqrt3;
const float sin30 = 0.5;
const float tan30 = 1/sqrt3;

// прямая кинематика: (theta1, theta2, theta3) -> (x0, y0, z0)
// возвращаемый статус: 0=OK, -1=несуществующая позиция
int delta_calcForward(float t1, float t2, float t3, float &x0, float &y0, float &z0) {
    float t = (f-e)*tan30/2;
    float dtr = pi/(float)180.0;

    t1 *= dtr;
    t2 *= dtr;
    t3 *= dtr;

    float y1 = -(t + rf*cos(t1));
    float z1 = -rf*sin(t1);

    float y2 = (t + rf*cos(t2))*sin30;
    float x2 = y2*tan60;
    float z2 = -rf*sin(t2);

    float y3 = (t + rf*cos(t3))*sin30;
    float x3 = -y3*tan60;
    float z3 = -rf*sin(t3);

    float dnm = (y2-y1)*x3-(y3-y1)*x2;

    float w1 = y1*y1 + z1*z1;
    float w2 = x2*x2 + y2*y2 + z2*z2;
    float w3 = x3*x3 + y3*y3 + z3*z3;

    // x = (a1*z + b1)/dnm
    float a1 = (z2-z1)*(y3-y1)-(z3-z1)*(y2-y1);
    float b1 = -((w2-w1)*(y3-y1)-(w3-w1)*(y2-y1))/2.0;

    // y = (a2*z + b2)/dnm;
    float a2 = -(z2-z1)*x3+(z3-z1)*x2;
    float b2 = ((w2-w1)*x3 - (w3-w1)*x2)/2.0;

    // a*z^2 + b*z + c = 0
    float a = a1*a1 + a2*a2 + dnm*dnm;
    float b = 2*(a1*b1 + a2*(b2-y1*dnm) - z1*dnm*dnm);
    float c = (b2-y1*dnm)*(b2-y1*dnm) + b1*b1 + dnm*dnm*(z1*z1 - re*re);

    // дискриминант
    float d = b*b - (float)4.0*a*c;
    if (d < 0) return -1; // несуществующая позиция

    z0 = -(float)0.5*(b+sqrt(d))/a;
    x0 = (a1*z0 + b1)/dnm;
    y0 = (a2*z0 + b2)/dnm;
    return 0;
}

// обратная кинематика
// вспомогательная функция, расчет угла theta1 (в плоскости YZ)
int delta_calcAngleYZ(float x0, float y0, float z0, float &theta) {
    float y1 = -0.5 * 0.57735 * f; // f/2 * tg 30
    y0 -= 0.5 * 0.57735 * e;       // сдвигаем центр к краю
    // z = a + b*y
    float a = (x0*x0 + y0*y0 + z0*z0 +rf*rf - re*re - y1*y1)/(2*z0);
    float b = (y1-y0)/z0;
    // дискриминант
    float d = -(a+b*y1)*(a+b*y1)+rf*(b*b*rf+rf);
    if (d < 0) return -1; // несуществующая точка
    float yj = (y1 - a*b - sqrt(d))/(b*b + 1); // выбираем внешнюю точку
    float zj = a + b*yj;
    theta = 180.0*atan(-zj/(y1 - yj))/pi + ((yj>y1)?180.0:0.0);
    return 0;
}

// обратная кинематика: (x0, y0, z0) -> (t1, t2, t3)
// возвращаемый статус: 0=OK, -1=несуществующая позиция
int delta_calcInverse(float x0, float y0, float z0, float &t1, float &t2, float &t3) {
    t1 = t2 = t3 = 0;
    int status = delta_calcAngleYZ(x0, y0, z0, t1);
    if (status == 0) status = delta_calcAngleYZ(x0*cos120 + y0*sin120, y0*cos120-x0*sin120, z0, t2);  // rotate coords to +120 deg
    if (status == 0) status = delta_calcAngleYZ(x0*cos120 - y0*sin120, y0*cos120+x0*sin120, z0, t3);  // rotate coords to -120 deg
    return status;
}
